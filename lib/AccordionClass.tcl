# ######################################################################
#
#  Copyright (c) Manfred ROSENBERGER, 2018
#
#      package: accordionMenu 	->	AccordionMenu.tcl
#
# ----------------------------------------------------------------------
#  namespace:  accordionMenu
# ----------------------------------------------------------------------
#

    # -----------------------------------------------------------------------------
    # -- style definitions required by AccordionMenu
    # -----------------------------------------------------------------------------
puts $tcl_version

ttk::style configure Higlighted.TButton \
        -background [ttk::style configure . -selectbackground]

ttk::style configure Left.Accordion -padding {3 0}
ttk::style layout \
    Left.Accordion {
        Button.button -children {
            Left.Accordion.focus  -children {
                Left.Accordion.padding -children {
                    Left.Accordion.label -sticky w
                }
            }
        }
    }
ttk::style layout \
    Right.Accordion {
        Button.button -children {
            Left.Accordion.focus  -children {
                Left.Accordion.padding -children {
                    Right.Accordion.label -sticky e
                }
            }
        }
    }

ttk::style configure Left.AccordionButton -padding {35 1 3 3}
ttk::style layout \
    Left.AccordionButton {
        Left.AccordionButton.button -children {
            Left.AccordionButton.focus -children {
                Left.AccordionButton.padding -children {
                    Left.AccordionButton.label -sticky w
                }
            }
        }
    }

ttk::style configure Left38.TCheckbutton -padding {38 1 3 3}
ttk::style configure Left45.TCheckbutton -padding {45 1 3 3}
ttk::style configure Left.TLabel         -padding {35 1 3 3}
    
    # -----------------------------------------------------------------------------
    # class base declarations
    # -----------------------------------------------------------------------------
oo::class create accordionMenu::AccordionClass {
        #
    variable ObjectPath
    variable ObjectRootPath
        #
    variable menuFrame
    variable configFrame
    variable itemDict
    variable configDict
    variable id
    variable id_Current
    variable id_Main
    variable id_Cmd
    variable itemMode
        #
    variable WidgetOptions
        #
    variable iconArray
    variable iconInternal
        #
    variable updateActive
        #
    constructor {path args} {
            #
        set updateActive       0    ;# -> itemClickEvent -> disabled  
            #
        set id                 0
        set id_Main            {}
        set id_Cmd             0
        set itemMode        _default_   ;# hides all other menues
            #
        set itemDict            {}
            # puts "    [self] path: $path"
            # puts "    [self] args: $args"
            #
        set configDict          {}
            #
            # declaration of all additional widget options
        array set WidgetOptions {
                -width        10
                -height      300
        }
            #
        array set WidgetOptions $args
            #
        array set iconArray     {}
            #
        array set iconInternal  {}  
        array set iconInternal  [list arrow_up      [image create photo -file $::accordionMenu::packageHomeDir/image/arrowup-a.gif]]
        array set iconInternal  [list arrow_down    [image create photo -file $::accordionMenu::packageHomeDir/image/arrowdown-a.gif]]
            #
        set ObjectRootPath      ${path} ;#  this will become a ttk::frame in BuildBase
            #
        ttk::frame $ObjectRootPath -class accordionMenu
            #
            #
        my BuildBase
            #
        my configure            {*}$args 
            #
        set args [my ExtractArgument $args -menu descAccordion]
            #
            # puts "        -> \$descAccordion $descAccordion"
            #
        if {[llength $descAccordion]} {
                #
            set configDict $descAccordion
            my BuildMenue
                #
        }
            #
        set updateActive       1    ;# -> itemClickEvent -> enabled
            #
    }

    destructor {
            # clean up once the widget get's destroyed
        set w [namespace tail [self]]
        catch {bind $w <Destroy> {}}
        catch {destroy $w}
            #
    }

    method unknown {method args} {
        return -code error "method \"$method\" is unknown"
    }

    method configure { args } {
            #
        if {[llength $args] == 0}  {
                #
                # as well as all custom options
            foreach xopt [array get WidgetOptions] {
                lappend opt_list $xopt
            }
            return $opt_list
                #
        } elseif {[llength $args] == 1}  {
                # return configuration value for this option
            set opt $args
            if { [info exists WidgetOptions($opt) ] } {
                return $WidgetOptions($opt)
            }
            return -code error "value for \"[lindex $args end]\" is not declared"
                #
        }
            #
            # error checking
        if {[expr {[llength $args]%2}] == 1}  {
            return -code error "value for \"[lindex $args end]\" missing"
        }
            #
            # process the new configuration options...
        array set opts $args
            #
        foreach opt_name [array names opts] {
                #
            set opt_value $opts($opt_name)
                # overwrite with new value
            if { [info exists WidgetOptions($opt_name)] } {
                set WidgetOptions($opt_name) $opt_value
            }
                #
        }
    }

    method BuildBase {} {
            #
        set menuFrame [ttk::frame $ObjectRootPath.frm]
        pack $menuFrame -padx 2 -pady 2 -fill both -expand true
        
            # The widget should not resize based on its content
        grid propagate $menuFrame 0
        
            # All panes fill the available horizontal space
        grid columnconfigure $menuFrame 0 -weight 1
            #
            #            
        set configFrame [ttk::frame $ObjectRootPath.cfg]
        pack $configFrame -padx 2 -pady 2 -fill x -expand false -side bottom
            #
        foreach opt_name [array names WidgetOptions] {
                #
                # some options need action from the widgets side
            switch -- $opt_name {
                -width  {
                    $menuFrame configure -width  $WidgetOptions(-width)
                }
                -height {
                    $menuFrame configure -height $WidgetOptions(-height)
                }
                default {}
            }
        }
            #
    }

    method ExtractArgument {args keystr argName} {
            #
        upvar $argName  classArg ;#  reference to the variable to be set at class-level
            #
        set classArg ""
        while {[set i [lsearch -exact $args $keystr]] >= 0} {
            set j        [expr $i + 1]
            set classArg [lindex $args $j]
            set args     [lreplace $args $i $j] ;#  remove index $i and $j from args
        }
            #
        return $args
            #
    }
        #
}

    # -----------------------------------------------------------------------------
    # class method declarations
    # -----------------------------------------------------------------------------
oo::define accordionMenu::AccordionClass {

    method createMenu {{cfgDict {}}} {    
            #
        if {$cfgDict != {}} {
            set configDict      $cfgDict
        }
            #
        my BuildMenue 
            #
        return            
            #
    }

    method addChapter {key keyDict args} {
            #
        set updateActive       0    ;# -> itemClickEvent -> disabled  
            #
        puts ""
        # puts "==========================================="
        # puts "    ... addChapter "
        # puts "      ... $args"
        # puts "$key"
        # puts "$keyDict"
        # puts "===== \$itemDict =========================="
        # puts "[dict print $itemDict]"
        # puts "===== \$configDict ========================"
        # puts "[dict print $configDict]"
        # puts "==========================================="

            #
        set label           [dict get $keyDict -label]
        set iconImage       [dict get $keyDict -iconImage]
        set command         [dict get $keyDict -command]
        set collapsable     "yes"
        catch {
        set collapsable     [dict get $keyDict -collapsable]
        }
            #
        set _image_  [image create photo]
        $_image_ copy $iconImage
        array set iconArray [list $key $_image_]
            #
        my AddChapter  $key  -text "$label"  -icon $_image_  -style "IconButton"  -collapsable $collapsable  -command [join $command]  {*}$args
            #
        if {[dict exists $keyDict children]} {
                #
            set childrenDict   [dict get $keyDict children]
                # puts "[dict print $childrenDict]"
                #
            my AddChildren $key $childrenDict
                #
        }
            #
        set updateActive       1    ;# -> itemClickEvent -> enabled
            #
    }

    method setMainChapter {key} {
            # puts "   setMainChapter: $key"
        set id_Main $key
        foreach _key [dict keys $itemDict] {
                # puts "         ... $_key"
            set itemContainer   [dict get $itemDict $_key itemContainer]
            grid remove $itemContainer
        }
        my showItem $key
    }
    
    method getChildren {} {
        return [winfo children $ObjectRootPath]
    }

    method disableItem {args} {
            #
            # puts "   -> disableItem $args"
        set keyPath [my GetKeyPath $args]
            # puts "   -> disableItem $keyPath"
            #
        dict set configDict {*}$keyPath -status disabled
            #
            # puts "   -> disableItem $args: [dict get $configDict {*}$keyPath]"
            #
        my BuildMenue
            #
    }

    method enableItem {args} {
            #
            # puts "   -> enableItem $args"
        set keyPath [my GetKeyPath $args]
            # puts "   -> enableItem $keyPath"
            #
        dict set configDict {*}$keyPath -status default
            #
            # puts "   -> enableItem $args: [dict get $configDict {*}$keyPath]"
            #
        my BuildMenue
            #
    }

    method showItem {id} {
            #
        if {$id ne {}} {
            my itemClickEvent $id
        } 
            #
    }

    method showMain {} {
            #
        if {$id_Main != {}} {
            my itemClickEvent $id_Main
        }
            #
        return $id_Main
            #
    }

    method itemClickEvent {id} {
            #
        if {!$updateActive} {
            return
        }
            #
            # puts "accordionMenu::AccordionClass -> itemClickEvent: $id"
            # puts "itemClickEvent: $id"
            # puts "$itemDict"
            #
        if {![dict exists $itemDict $id itemCmd]} {
            return
        }
            #
        set itemCmd         [dict get $itemDict $id itemCmd]
        set id_Current      $id
            # ---------
            # puts "    -> \$itemCmd: $itemCmd"
        if {[catch {{*}$itemCmd} eID]} {
            puts "\n    ... <E> could not execute: $itemCmd\n          -> $eID\n"
        }
            #
            # ---------
        my Update
            #
    }        

    method expandcollapseClickEvent {args} {
            #
        if {$itemMode eq {_default_}} {
            set itemMode {_open_}
            my AddCollapseButton
        } else {
            set itemMode {_default_}
            my DestroyCollapseButton
        }    
            #
        my Update 
            #
    }

    method getConfigDict {} {
        return $configDict
    }

    method showToolTip {widget text} {
            #
            # http://wiki.tcl.tk/1954 
            #
        global tcl_platform
            #
        if { [string match $widget* [winfo containing  [winfo pointerx .] [winfo pointery .]] ] == 0  } {
                return
        }
            #
        catch { destroy $widget.tooltip }
            #
        set scrh [winfo screenheight $widget]    ; # 1) flashing window fix
        set scrw [winfo screenwidth $widget]     ; # 1) flashing window fix
        set tooltip [toplevel $widget.tooltip -bd 1 -bg black]
        wm geometry $tooltip +$scrh+$scrw        ; # 1) flashing window fix
        wm overrideredirect $tooltip 1
            #
        if {$tcl_platform(platform) == {windows}} { ; # 3) wm attributes...
            wm attributes $tooltip -topmost 1       ; # 3) assumes...
        }                                           ; # 3) Windows
        pack [label $tooltip.label -bg lightyellow -fg black -text $text -justify left]
            #
        set width  [winfo reqwidth $tooltip.label]
        set height [winfo reqheight $tooltip.label]
            # 
            # b.) Is the pointer in the bottom half of the screen?
        set pointer_below_midline [expr [winfo pointery .] > [expr [winfo screenheight .] / 2.0]]                
            #
            # c.) Tooltip is centred horizontally on pointer.
            #     default x: - round($width / 2.0)
            #          set positionX [expr [winfo pointerx .] - round($width / 2.0)] 
        set positionX [expr [winfo pointerx .] +  5] 
            # b.) Tooltip is displayed above or below depending on pointer Y position.
        set positionY [expr [winfo pointery .] + 25 * ($pointer_below_midline * -2 + 1) - round($height / 2.0)]  
            #
            # a.) Ad-hockery: Set positionX so the entire tooltip widget will be displayed.
            # c.) Simplified slightly and modified to handle horizontally-centred tooltips and the left screen edge.
        if  {[expr $positionX + $width] > [winfo screenwidth .]} {
                set positionX [expr [winfo screenwidth .] - $width]
        } elseif {$positionX < 0} {
                set positionX 0
        }

        wm geometry $tooltip [join  "$width x $height + $positionX + $positionY" {}]
        raise $tooltip

            # 2) Kludge: defeat rare artifact by passing mouse over a tooltip to destroy it.
        bind $widget.tooltip <Any-Enter> {destroy %W}
        bind $widget.tooltip <Any-Leave> {destroy %W}
            #
    }

    
        #
    method add {args} {
            #
            # puts "    -> add $args"
        incr id
            #
        set args            [my ExtractArgument $args -text      buttonText]
        set args            [my ExtractArgument $args -command   itemCmd]
            #
        if 0 {
            if [winfo exists $menuFrame.pf_$id] {
                puts "    add: check  [winfo children $menuFrame.pf_$id]"
            }
        }
        set itemFrame       [ttk::frame $menuFrame.pf_$id]
        if 0 {
            puts "----------------- [self] <- $itemFrame"
        }
        set image           {}
        set style           Left.Accordion
        set args            [my ExtractArgument $args -image     image]
        set args            [my ExtractArgument $args -style     style]
        if {$image != {}} {
            set itemButton  [ttk::button $itemFrame.button \
                                -text $buttonText \
                                -command [list [self] itemClickEvent $id] \
                                -image $image \
                                -style $style \
                                -compound left]
        } else {
            set itemButton  [ttk::button $itemFrame.button \
                                -text $buttonText \
                                -command [list [self] itemClickEvent $id] \
                                -style $style]
        }                        
        if 0 {
            if [winfo exists $itemFrame] {
                puts "    add: check  [winfo children $itemFrame]"
            }
        }
        set itemContainer   [ttk::frame $itemFrame.container]
        if 0 {
            puts "    add: check  $itemFrame"
        }
            #
        dict set            itemDict $id itemCmd          $itemCmd
        dict set            itemDict $id itemFrame        $itemFrame
        dict set            itemDict $id itemButton       $itemButton
        dict set            itemDict $id itemContainer    $itemContainer
            #
        grid $itemFrame     -in $menuFrame      -sticky ew
        grid $itemButton    -in $itemFrame      -sticky ew  -columnspan 2
            #
        grid $itemContainer -in $itemFrame      -sticky ew
            #
        grid columnconfigure $itemFrame $itemButton     -weight  1  
        grid columnconfigure $itemFrame $itemContainer  -weight 28
            #
            # -- remove for now, itemContainer is managed again by itemClickEvent
        if {$itemMode eq {_default_}} {
            grid remove $itemContainer
        }
            #
    }    
        #
    method addCommand {id args} {
            #
            # ... old style
            #
        incr id_Cmd
            #
            # puts "   addCommand"
            # puts "      -> $id"
            # puts "      -> $args"
            #
        set args            [my ExtractArgument $args -type      widgetCommand]
        set args            [my ExtractArgument $args -tooltip   toolTip]
            #
        set itemContainer   [dict get $itemDict $id     itemContainer]
            #
            # puts "   -> addCommand -> \$itemContainer $itemContainer . w_$id_Cmd"
            #
        switch -exact $widgetCommand {
            button {
                set w   [ttk::$widgetCommand    $itemContainer.w_$id_Cmd  {*}$args -style Left.AccordionButton]
            }
            checkbutton {
                set w   [ttk::$widgetCommand    $itemContainer.w_$id_Cmd  {*}$args -style Left38.TCheckbutton]
            }
            checkbutton45 {
                set w   [ttk::checkbutton       $itemContainer.w_$id_Cmd  {*}$args -style Left45.TCheckbutton]
            }
            label {
                set w   [ttk::$widgetCommand    $itemContainer.w_$id_Cmd  {*}$args -style Left.TLabel]
            }
            separator {
                set w   [ttk::frame             $itemContainer.w_$id_Cmd  -padding {10 0 5 0}]
                set sep [ttk::$widgetCommand    $w.sep_$id_Cmd            {*}$args]
                pack $sep  -fill x  -expand yes
            }
            default {
                set w   [$widgetCommand  $itemContainer.w_$id_Cmd  {*}$args]
            }
        }
        
            #
        grid $w             -in $itemContainer  -sticky ew
        grid columnconfigure $itemContainer $w     -weight 1
            #
            # open just this item
        my showItem $id    
            #
        my SetTooltip $w $toolTip
            #
    }
        #
    method addConfig {command} {
            #
            # ... old style
            #
        if {$command eq {}} {
            set command [list [self] expandcollapseClickEvent {}]
        }
            #
        set itemButton      [ttk::button $configFrame.button \
                                -text " ... expand / collapse" \
                                -command $command \
                                -style Right.Accordion]
            #
        pack $itemButton    -fill x
            #
    }
        #
    method getPath {} {
        return $ObjectRootPath 
    }
        #
    
        #
    method report {} {
        puts "===== \$itemDict =========================="
        puts "[dict print $itemDict]"
        puts "===== \$itemDict =========================="
        puts "[dict print $itemDict]"
        puts "===== \$configDict ========================"
        puts "[dict print $configDict]"
        puts "==========================================="
            #
        puts "  ... \$ObjectRootPath    $ObjectRootPath    "
        #
        puts "  ... \$menuFrame         $menuFrame         "
        puts "  ... \$configFrame       $configFrame       "
        puts "  ... \$itemDict          $itemDict          "
        puts "  ... \$configDict        $configDict        " 
            
            
            
    }
        #

    # ---------------
    # private methods
    # ---------------

    method CleanupBase {} {
            #
        destroy             $menuFrame
        destroy             $configFrame
            #
            #
        set id              0
        set id_Cmd          0
        set id_Parent       0
            #
        set itemDict        {}
            #
        foreach key [array names iconArray] {
                # puts "     -> $key $iconArray($key)"
            catch {image delete    $iconArray($key)}
        }
        array set iconArray {}
            #
            #
        my BuildBase
            #
    }

    method BuildMenue {} {
            #
            # puts "\n==========================================================="
            # puts "  --> BuildMenue"
            # puts "   -> \$ObjectRootPath $ObjectRootPath"
            # puts "    -> \$configFrame   $configFrame"
            # puts "    -> \$menuFrame     $menuFrame"
            # puts "----------------------------------------------"
            #
        my CleanupBase
            #
        array set iconArray {}
            #
        dict for {key keyDict} $configDict {    
                #
                # puts [dict print $keyDict]
                #
            set label           [dict get $keyDict -label]
            set iconImage       [dict get $keyDict -iconImage]
            set command         [dict get $keyDict -command]
            set isMain          "no"
            catch {
                set isMain      [dict get $keyDict -main]
            }
                #
            if {[my GetConfigStatus $keyDict] eq {disabled}} {
                    # puts "     ... skipped: -text $label  -image $iconArray($key)  -style IconButton  -command [join $command]"
                continue
            } else {
                set _image_  [image create photo]
                $_image_ copy $iconImage
                array set iconArray [list $key $_image_]
                my AddChapter  $key  -text "$label"  -icon $_image_  -style "IconButton"  -main $isMain  -command [join $command]
                    # Chapter  $key  -text "$label"  -icon $iconImage  -style "IconButton"  -command [join $command]
            }
                #
                #
            if {[dict exists $keyDict children]} {
                    #
                set childrenDict   [dict get $keyDict children]
                    # puts "[dict print $childrenDict]"
                    #
                my AddChildren  $key $childrenDict
                    #
            }
                #
        }
            #
            #
            # -- final config -----------------------
            #
        my AddExpandCollapseButton
            #
    }

    method AddChapter {id args} {
            #
            puts "    -> AddChapter $id $args"
            # incr id
            #
        set idBefore    {}
        set idAfter     {}
        set itemRef     {}
        set isMain      no
        set appendType  end
            #
        set args            [my ExtractArgument $args -text         buttonText]
        set args            [my ExtractArgument $args -command      itemCmd]
        set args            [my ExtractArgument $args -before       idBefore]
        set args            [my ExtractArgument $args -after        idAfter]
        set args            [my ExtractArgument $args -main         isMain]
            # set args      [my ExtractArgument $args -collapsable  itemCollapsable]
            #
        if {$isMain eq {yes}} {
            set id_Main     $id
        }
            #
        if {$idAfter != {}} {
            set itemRef     $menuFrame.pf_$idAfter
            set appendType  after
            if {[winfo exists $itemRef]} {
                set appendType  after
            }
        }
            # prefer -before 
        if {$idBefore != {}} {
            set itemRef     $menuFrame.pf_$idBefore
            if {[winfo exists $itemRef]} {
                set appendType  before
            }
        }

            #
        set itemFrame       [ttk::frame $menuFrame.pf_$id]
            #
            # puts "----------------- [self] <- $itemFrame"
            #
        set image           {}
        set style           Left.Accordion
        set args            [my ExtractArgument $args -icon     iconImage]
        set args            [my ExtractArgument $args -style    style]
            #
        if {$iconImage != {}} {
                #
            array set iconArray [list $id $iconImage]
                #
            set itemButton \
                [ttk::button $itemFrame.button \
                        -text $buttonText \
                        -command [list [self] itemClickEvent $id] \
                        -image $iconArray($id) \
                        -style $style \
                        -compound left]
                #
        } else {
                #
            set itemButton \
                [ttk::button $itemFrame.button \
                        -text $buttonText \
                        -command [list [self] itemClickEvent $id] \
                        -style $style]
                #
        }
            #
        set itemContainer   [ttk::frame $itemFrame.container]
            #
        dict set            itemDict $id itemCmd          $itemCmd
        dict set            itemDict $id itemFrame        $itemFrame
        dict set            itemDict $id itemButton       $itemButton
        dict set            itemDict $id itemContainer    $itemContainer
            # dict set      itemDict $id itemCollapsable  $itemCollapsable
            #
            #
        switch -exact -- $appendType {
            before {
                my InsertChapterWidget  $itemFrame  before $itemRef
                    # grid $itemFrame     -in $menuFrame      -sticky ew  -row 2
            }
            after {
                my InsertChapterWidget  $itemFrame  after  $itemRef
                    # grid $itemFrame     -in $menuFrame      -sticky ew  -row 3
            }
            end -
            default {
                my InsertChapterWidget  $itemFrame  end
                    # grid $itemFrame     -in $menuFrame      -sticky ew
            }
        }
            #
        grid $itemButton    -in $itemFrame      -sticky ew  -columnspan 2
        grid $itemContainer -in $itemFrame      -sticky ew
            #
        grid columnconfigure $itemFrame $itemButton     -weight  1  
        grid columnconfigure $itemFrame $itemContainer  -weight 28
            #
            # -- remove for now, itemContainer is managed again by itemClickEvent
        if {$itemMode eq {_default_}} {
            grid remove $itemContainer
        }
            #
    }    

    method AddChildren {id dictChildren} {
            #
        set key $id
            #
        dict for {childKey childDict} $dictChildren {
                #
                # puts "\n---> \$childKey $childKey"
                # puts "[dict print $childDict]"
                #
            if {[my GetConfigStatus $childDict] eq {disabled}} {
                    # puts "     ... skipped: -text $label  -command [join $command]"
                continue
            }
                #
            set typeChild   [dict get   $childDict -type]
                #
            switch -exact -- $typeChild {
                button {
                        #
                    set label       [dict get $childDict -label] 
                    set command     [dict get $childDict -command] 
                    set tooltip     [dict get $childDict -tooltip] 
                        #
                    my AddChild  $childKey  $key  -type button       -text $label  -command [join $command]  -tooltip $tooltip
                        #
                }
                separator {
                        #
                    my AddChild  $childKey  $key  -type separator
                        #
                }
                label {
                        #
                    set label       [dict get $childDict -label] 
                        #
                    my AddChild  $childKey  $key  -type label        -text $label
                        #
                }
                checkbutton {
                        #
                    set label       [dict get $childDict -label] 
                    set command     [dict get $childDict -command] 
                    set variable    [dict get $childDict -variable] 
                        #
                    my AddChild  $childKey  $key  -type checkbutton  -text $label  -command [join $command]  -variable $variable                    
                        #
                }
                default {
                }
            }
        }
            #
    }

    method AddChild {id parentID args} {
            #
            # puts "   addCommand"
            # puts "      -> $id"
            # puts "      -> $args"
            #
        set toolTip         ""
            #
        set args            [my ExtractArgument $args -type     widgetCommand]
        set args            [my ExtractArgument $args -tooltip  toolTip]
            #
            #
            # puts "      -> \$parentID $parentID"
            # puts "      -> \$id       $id"
            # puts "      -> \$args     $args"
            # puts "      -> \$itemDict $itemDict"
            #
        set itemContainer   [dict get $itemDict $parentID   itemContainer]
            #
            # puts "   -> addCommand -> \$itemContainer $itemContainer.w_$id"
            #
        switch -exact $widgetCommand {
            button {
                set w   [ttk::button        $itemContainer.w_$id    {*}$args  -style Left.AccordionButton]
            }
            checkbutton {
                set w   [ttk::checkbutton   $itemContainer.w_$id    {*}$args  -style Left38.TCheckbutton]
            }
            checkbutton45 {
                set w   [ttk::checkbutton   $itemContainer.w_$id    {*}$args  -style Left45.TCheckbutton]
            }
            label {
                set w   [ttk::label         $itemContainer.w_$id    {*}$args  -style Left.TLabel]
            }
            separator {
                set w   [ttk::frame         $itemContainer.w_$id    -padding {10 0 5 0}]
                set sep [ttk::separator     $w.sep_$id              {*}$args]
                pack $sep  -fill x  -expand yes
            }
            default {
                set w   [$widgetCommand     $itemContainer.w_$id    {*}$args]
            }
        }
        
            #
        grid $w             -in $itemContainer      -sticky ew
        grid columnconfigure    $itemContainer $w   -weight 1
            #
            # open just this item
        my showItem         $parentID
            #
            # puts "     -> \$w $w"
        my SetTooltip       $w $toolTip
            #
    }

    method InsertChapterWidget {chapterWidget location {referenceWidget {}}} {
            #
            # puts "  InsertChapterWidget"
            # puts "        \$chapterWidget   $chapterWidget"
            # puts "        \$location        $location"
            # puts "        \$referenceWidget $referenceWidget"
            # puts "          \$referenceWidget $referenceWidget  ... exists: [winfo exists $referenceWidget]"
            #
        set listSlaves  [pack slave $menuFrame]
            #
            # puts "        \$listSlaves:     $listSlaves"
            #
        if {[lsearch -exact $listSlaves $chapterWidget] >= 0} {
                #
            return error -errorinfo "widget $chapterWidget allready packed in $menuFrame"
                #
        } else {
                # puts "   -- check --"
                # puts "          location: $location"
                # puts "          chapterWidget: $chapterWidget"
            switch -exact -- $location {
                before {
                    pack $chapterWidget -fill x -before $referenceWidget
                }
                after {
                    pack $chapterWidget -fill x -after  $referenceWidget
                }
                end -
                default {
                    pack $chapterWidget -fill x
                }
            }
        }
            #
        set listSlaves  [winfo children $menuFrame]
            #
            # puts "          ----> $listSlaves"
            #
        return
            #
    }

    method Update {} {
            # ---------
            # puts "    -> $id_Current / $id"
            #
        foreach key [dict keys $itemDict] {
            set itemFrame       [dict get $itemDict $key itemFrame]
            set itemContainer   [dict get $itemDict $key itemContainer]
                # set itemCollapsable [dict get $itemDict $key itemCollapsable]
                # puts "$itemFrame"
                # puts "$itemFrame"
                # puts "         $key <- $id_Main"
                # puts "$itemContainer"
            if {$key eq $id_Main} {
                grid $itemContainer -in $itemFrame  -sticky ew
                    # puts "             $key <- \$id_Main <- $id_Main ... expand"
                continue
            }
                #
            if {$itemMode eq {_default_}} {
                if {$key eq $id_Current} {
                    grid $itemContainer -in $itemFrame  -sticky ew
                } else {
                    grid remove $itemContainer
                }
            } else {
                grid $itemContainer -in $itemFrame  -sticky ew
            }
                #
        }
            #
    }

    method GetKeyPath {args} {
            #
        if {[llength $args]} {
            lassign [join $args] _key_ _childKey_
        }
            #
            # puts "   -> GetKeyPath $args"
            # puts "   -> GetKeyPath \$_key_      $_key_"
            # puts "   -> GetKeyPath \$_childKey_ $_childKey_"
            #
        dict for {key keyDict} $configDict {
                # puts "   -> $key"
            if {$key eq $_key_} {
                    #
                if [dict exists $keyDict children] {
                    dict for {key2 keyDict2} [dict get $keyDict children] {
                            # puts "       -> children $key2"
                        if {$key2 eq $_childKey_} {
                                # puts "     ---> [list $_key_ children $_childKey_]"
                            return [list $_key_ children $_childKey_]
                        }
                    }
                }
                    #
                return $_key_
                    #
            }
        }        
            #
    }

    method GetConfigStatus {branchDict} {
            #
        if {[dict exists $branchDict -status]} {
            if {[dict get $branchDict -status] eq {disabled}} {
                return disabled
            } else {
                return default
            }
        } else {
            return default
        }
            #
    }

    method SetTooltip {widget text} {
            # http://wiki.tcl.tk/1954
            #   setTooltip $widget "Hello World!"
            #   ttk::button does not support -helptext of Button in BWidget
            # puts "\n -- SetTooltip --" 
            # puts "       self [self]"
            # puts "       self [info object class [self]] "
            # puts "       self [info object methods [self] -all] "
            # puts "       \$ObjectPath $ObjectPath"
            #
        if { $text != "" } {
            # 2) Adjusted timings and added key and button bindings. These seem to
            # make artifacts tolerably rare.
              #  $widget <Any-Enter>    [list after 300 [list [self] showToolTip %W $text]]
            bind $widget <Any-Enter>    [list after 300 [list [self] showToolTip %W $text]]
            bind $widget <Any-Leave>    [list after 300 [list destroy %W.tooltip]]
            bind $widget <Any-KeyPress> [list after 300 [list destroy %W.tooltip]]
            bind $widget <Any-Button>   [list after 300 [list destroy %W.tooltip]]
        }
            #
    }

    method AddExpandCollapseButton {} {
            #
        set command         [list [self] expandcollapseClickEvent {}]
            #
        set itemButton      [ttk::button $configFrame.button \
                                -text " ... expand / collapse " \
                                -command $command \
                                -style Right.Accordion]
            #
        pack $itemButton    -fill x
            #
    }
        #
    method AddCollapseButton {} {
            #
        set command     [list [self] expandcollapseClickEvent {}]
            #
        set itemButton      [button $menuFrame.buttonCollapse \
                                -image $iconInternal(arrow_up) \
                                -bd 0 \
                                -command $command]
            #
        place $itemButton   -in $menuFrame  -anchor ne  -relx 1.0  -x -2  -y 2
            #
    }
        #
    method DestroyCollapseButton {} {
        destroy $menuFrame.buttonCollapse
    }
        #
}
