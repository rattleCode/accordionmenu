# -----------------------------------------------------------------------------
# -- accordionMenu.tcl
# -----------------------------------------------------------------------------
# Credits:
#    Idea carried over from the original accordion.tm file
#    Thanks to:
#    Copyright (c) 2014, Schelte Bron <sbron@users.sourceforge.net>
# -----------------------------------------------------------------------------
#    2016, Johann Oberdorfer - Engineering Support | CAD | Software
#    johann.oberdorfer [at] gmail.com, www.johann-oberdorfer.eu
# -----------------------------------------------------------------------------
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
# -----------------------------------------------------------------------------
#    2018, Manfred Rosenberger - rattleCAD
#    manfred.rosenberger [at] gmx.net, rattleCAD.sourceforge.net
# ----------------------------------------------------------------------
# 
#    2018/07/31
#
# ----------------------------------------------------------------------
#  namespace:  accordionMenu
# ----------------------------------------------------------------------
#
#

    #
package provide accordionMenu 0.04.06
    #
package require Tk
package require TclOO
    #
    # -----------------------------------------------------------------------------------
    #
    # namespace:    a c c o r d i o n M e n u
    #

namespace eval accordionMenu {
        # --------------------------------------------
        #
    variable packageHomeDir [file dirname [file normalize [info script]]]
        #
}
