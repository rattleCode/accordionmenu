

    proc extractArgument {args keystr argName} {
            #
        upvar $argName classArg ;#  reference to the variable $argName to be set at class-level
            #
        set classArg ""
        while {[set i [lsearch -exact $args $keystr]] >= 0} {
            set j        [expr $i + 1]
            set classArg [lindex $args $j]
            set args     [lreplace $args $i $j]
        }
            #
        puts "      <-A-> \$classArg $classArg"
        return $args
            #
    }
    

set args [list -a 01 -b 02 -c "03 04" -d abcdef]

puts "  -000-   $args"

set args [extractArgument $args -a myVar]

puts "  -001-   $args    -> -a $myVar"
