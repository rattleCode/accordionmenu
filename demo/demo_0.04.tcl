 ########################################################################
 #
 # demo_00.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2018/07/31
 #
 #

    #
set TEST_ROOT_Dir    [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Image_Dir   [file join $TEST_ROOT_Dir _image]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
    #
puts " ------------"
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Image_Dir ..... $TEST_Image_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
puts " ------------"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
#lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
puts " ------------"
    
    
proc init_iconArray {} {   
        #
    variable iconArray
        #
    array set iconArray [list A  [image create photo _image_a_ -file $::TEST_Image_Dir/A.png]]
    array set iconArray [list B  [image create photo _image_b_ -file $::TEST_Image_Dir/B.png]]
    array set iconArray [list C  [image create photo _image_c_ -file $::TEST_Image_Dir/C.png]]
    array set iconArray [list D  [image create photo _image_d_ -file $::TEST_Image_Dir/D.png]]
    array set iconArray [list E  [image create photo _image_e_ -file $::TEST_Image_Dir/E.png]]
    array set iconArray [list F  [image create photo _image_f_ -file $::TEST_Image_Dir/F.png]]
    array set iconArray [list G  [image create photo _image_g_ -file $::TEST_Image_Dir/G.png]]
        #
}
    #

    #
proc update_ttkStyle {} {
        #
    catch {package require ttkThemes}
            #
    foreach name [ttk::themes] {
        puts "    ... $name"
    }
        #
    if {[lsearch -exact [package names] ttk::theme::clearlogic] > -1} {
            #
        puts "\n    ... package ttk::theme::clearlogic available\n"
            #
        ttk::setTheme clearlogic
            #
    } 
        #
    puts "\n    ... current theme: [ttk::style theme use]\n"
        #
    if {[catch {set layout [ttk::style layout IconButton]} eId]} {
        puts "    ... style IconButton ... does not exist"
        puts "        ... $eId"
        ttk::style configure IconButton -padding {0 0}
        ttk::style layout IconButton {
            IconButton.button -children {
                IconButton.focus -children {
                    IconButton.padding -children {
                        IconButton.label -sticky nsew
                    }
                }
            }
        }
    } else {
        puts "    ... style IconButton:"
        puts "        $layout"
    }
        #
}            
    #
proc create_AccordionMenu_old {parentFrame} {    
        #
        #
    variable        objAccordion_A
    variable        iconArray
        #
        #
        #
    set baseFrame   [ttk::frame $parentFrame.base_a]
        #
        # return
        #
        # set acc   [accordionMenu  $baseFrame.a -width 210 -speed 900]
    set acc         [accordionMenu::AccordionClass new $baseFrame.a -width 210 -speed 900]
    set accWidget   [$acc getPath]
    pack $accWidget  -fill both  -expand yes
        #
    pack $baseFrame -fill both  -expand yes  -side top
        #
        #
        #
        # -- Project ----------------------------
        #
    $acc add            -text "Project"         -image $iconArray(A)        -style "IconButton"     -command [list ::updateCmd project]  
        #
    $acc addCommand 1   -type button        -text "open Project ..."             -command [list ::updateCmd project_01]   -tooltip "... open Project"             
    $acc addCommand 1   -type button        -text "read Template ..."            -command [list ::updateCmd project_02]   -tooltip "... read Template"            
    $acc addCommand 1   -type separator
    $acc addCommand 1   -type button        -text "Template Road"                -command [list ::updateCmd project_03]   -tooltip "... use Template Road"        
    $acc addCommand 1   -type button        -text "Template Off-Road"            -command [list ::updateCmd project_04]   -tooltip "... use Template Off-Road"    
        #
        #
        #
        # -- BikeFitting ------------------------
        #
    $acc add            -text "Bike Fitting"    -image $iconArray(B)        -style "IconButton"     -command [list ::updateCmd fitting]
        #
    $acc addCommand 2   -type button       -text "enter Position"              -command [list ::updateCmd fitting_01]    -tooltip "... enter positions from bike-fitting"
        #
        #
        #
        # -- Geometry ---------------------------
        #
    $acc add            -text "Geometry"        -image $iconArray(C)        -style "IconButton"     -command [list ::updateCmd geometry]
        #
    $acc addCommand 3   -type button       -text "import Bike Fitting"         -command [list ::updateCmd geometry_01]   -tooltip "... import positions from bike-fitting as reference"
    $acc addCommand 3   -type button       -text "import Frame"                -command [list ::updateCmd geometry_02]   
    $acc addCommand 3   -type separator                                    
    $acc addCommand 3   -type label        -text "Dimensions:"
    $acc addCommand 3   -type checkbutton  -text "Secondary"                   -command [list ::updateCmd geometry_03]   -variable ::show_secondaryDimension
    $acc addCommand 3   -type checkbutton  -text "Result"                      -command [list ::updateCmd geometry_04]   -variable ::show_resultDimension   
    $acc addCommand 3   -type checkbutton  -text "Summary"                     -command [list ::updateCmd geometry_05]   -variable ::show_summaryDimension  
        #
        #
        #
        # -- Frame & Tubes ----------------------
        #
    $acc add            -text "Frame & Frame Tubes"  -image $iconArray(D)   -style "IconButton"     -command [list ::updateCmd frametubes]
        #
    $acc addCommand 4   -type button       -text "Frame Details"               -command [list ::updateCmd frametubes_01] -tooltip "... refine frame & frame tubes"
    $acc addCommand 4   -type button       -text "ChainStays Details"          -command [list ::updateCmd frametubes_02] -tooltip "... refine chainstays"
    $acc addCommand 4   -type separator    
    $acc addCommand 4   -type button       -text "check Lug Angles"            -command [list ::updateCmd frametubes_03] -tooltip "... check lug-angles for building with lugs"
        #
        #
        #
        # -- Summary ----------------------------
        #
    $acc add            -text "Summary & Mockup"    -image $iconArray(E)    -style "IconButton"     -command [list ::updateCmd mockup]
        #
    $acc addCommand 5   -type button       -text "Summary"                     -command [list ::updateCmd mockup_01]     -tooltip "... show complete mockup with main dimensions"
    $acc addCommand 5   -type button       -text "Mockup"                      -command [list ::updateCmd mockup_02]     -tooltip "... without dimensions"
    $acc addCommand 5   -type separator
    $acc addCommand 5   -type button       -text "edit Rendering"              -command [list ::updateCmd mockup_11]     -tooltip "... select main components"
        #
        #
        #
        # -- Workshop ---------------------------
        #
    $acc add            -text "Workshop Drawings"   -image $iconArray(F)    -style "IconButton"    -command [list ::updateCmd workshop]
        #
    $acc addCommand 6   -type button       -text "Frame Drafting"              -command [list ::updateCmd workshop_01]   -tooltip "... view workshop drawing of frame"
    $acc addCommand 6   -type button       -text "Tube Miter"                  -command [list ::updateCmd workshop_02]   -tooltip "... view flattend tube miterings"
    $acc addCommand 6   -type button       -text "Frame Jig"                   -command [list ::updateCmd workshop_03]   -tooltip "... select your frame jig"
        #
        #
        #
        # -- Export ---------------------------
        #
    $acc add            -text "Overview & Export"   -image $iconArray(G) -style "IconButton"     -command [list ::updateCmd export]
        #
    $acc addCommand 7   -type button       -text "show all Tabs"               -command [list ::updateCmd export_00] 
    $acc addCommand 7   -type separator    
    $acc addCommand 7   -type button       -text "export PDF"                  -command [list ::updateCmd export_01]     -tooltip "... export project as PDF"
    $acc addCommand 7   -type button       -text "export HTML"                 -command [list ::updateCmd export_02]     -tooltip "... export project as HTML"
    $acc addCommand 7   -type separator    
    $acc addCommand 7   -type button       -text "export SVG"                  -command [list ::updateCmd export_03]     -tooltip "... export single vies as SVG"
        #
        #
        #
        # -- final config -----------------------
        #
    $acc addConfig  {}
        #
        # -- finish -----------------------------
        #
    set objAccordion_A $acc
        #
    return
        #
}
    #
proc create_AccordionMenu {parentFrame configDict} {    
        #
        #
    variable        objAccordion_B
    variable        iconArray
        #
        #
        #
    set baseFrame       [ttk::frame $parentFrame.base_a]
        #
    # set objAccordion_B  [accordionMenu  $baseFrame.a -width 210 -speed 900]
    set objAccordion_B  [accordionMenu::AccordionClass new $baseFrame.a -width 210 -speed 900]
    set accWidget       [$objAccordion_B getPath]
    pack $accWidget  -fill both  -expand yes
        #
    puts "      -> \$objAccordion_B $objAccordion_B"
        #
    pack $baseFrame -fill both  -expand yes  -side top
        #
    $objAccordion_B     createMenu $configDict
        #
    puts "      -> \$objAccordion_B $objAccordion_B -> [$objAccordion_B getChildren]"
        #
        #
    return    
        #
}
    #
proc updateCmd {id args} {

    variable        objAccordion_A
    variable        objAccordion_B
    
    variable        iconArray

    .f2.t delete 1.0 end
    
    switch -exact $id {

        export          {.f2.t insert end $id}
        export_00       {.f2.t insert end $id}
        export_01       {.f2.t insert end $id}
        export_02       {.f2.t insert end $id}
        export_03       {.f2.t insert end $id}
        fitting         {.f2.t insert end $id}
        fitting_01      {.f2.t insert end $id}
        frametubes      {.f2.t insert end $id}
        frametubes_01   {.f2.t insert end $id}
        frametubes_02   {.f2.t insert end $id}
        frametubes_03   {.f2.t insert end $id}
        geometry        {.f2.t insert end $id}
        geometry_01     {.f2.t insert end $id}
        geometry_02     {.f2.t insert end $id}
        geometry_03     {.f2.t insert end $id}
        geometry_04     {.f2.t insert end $id}
        geometry_05     {.f2.t insert end $id}
        mockup          {.f2.t insert end $id}
        mockup_01       {.f2.t insert end $id}
        mockup_02       {.f2.t insert end $id}
        mockup_11       {.f2.t insert end $id}
        project         {.f2.t insert end $id}
        project_01      {.f2.t insert end $id}
        project_02      {.f2.t insert end $id}
        project_03      {.f2.t insert end $id}
        project_04      {.f2.t insert end $id}
        workshop        {.f2.t insert end $id}
        workshop_01     {.f2.t insert end $id}
        workshop_02     {.f2.t insert end $id}
        workshop_03     {.f2.t insert end $id}
        
        report {
                .f2.t insert end $id
                .f2.t insert end " ... report"
                $objAccordion_B report
            }
        
        getConfigDict {
                set configDict [$objAccordion_B getConfigDict]
                .f2.t insert end $id
                .f2.t insert end [dict print $configDict]
            }
                        
        hideEntry {
                .f2.t insert end $id
            }
                        
        disableEntry {
                .f2.t insert end $id
                $objAccordion_B disableItem A 02
                set configDict [$objAccordion_B getConfigDict]
                .f2.t insert end [dict print $configDict]
            }
        enableEntry {
                .f2.t insert end $id
                $objAccordion_B enableItem A 02
                set configDict [$objAccordion_B getConfigDict]
                .f2.t insert end [dict print $configDict]
            }
        
        disableGeometry {
                .f2.t insert end $id
                $objAccordion_B disableItem C
                set configDict [$objAccordion_B getConfigDict]
                .f2.t insert end [dict print $configDict]
            }
        
        enableGeometry {
                .f2.t insert end $id
                $objAccordion_B enableItem C
                set configDict [$objAccordion_B getConfigDict]
                .f2.t insert end [dict print $configDict]
            }
        
        enableCustom {
                .f2.t insert end $id
                $objAccordion_B enableItem D
                set configDict [$objAccordion_B getConfigDict]
                .f2.t insert end [dict print [dict get $configDict D]]
            }
        
        disableCustom {
                .f2.t insert end $id
                $objAccordion_B disableItem D
                set configDict [$objAccordion_B getConfigDict]
                .f2.t insert end [dict print [dict get $configDict D]]
            }
        
        openGeometry {
                .f2.t insert end $id
                $objAccordion_B showItem C
            }
                        
        addMenu_F_before_E {
                    #
                set menu_Add  [list \
                    -label          {Add Chapter F before E} \
                    -iconImage      $iconArray(F) \
                    -command        {::updateCmd frametubes}\
                    children {
                        Add_F_01 {
                            -type       button \
                            -label      "add - Frame Details" \
                            -command    {::updateCmd frametubes_01} \
                            -tooltip    "... refine frame & frame tubes" \
                        }
                        Add_F_02 {
                            -type       button \
                            -label      "add - ChainStays Details" \
                            -command    {::updateCmd frametubes_01} \
                            -tooltip    "... refine chainstays" \
                        }
                        Add_F_03 {
                            -type       separator \
                        }
                        Add_F_04 {
                            -type       button \
                            -label      "add - check Lug Angles" \
                            -command    {::updateCmd frametubes_01} \
                            -tooltip    "... check lug-angles for building with lugs" \
                        }
                    }
                ]
                    #    
                $objAccordion_B addChapter Add_F $menu_Add    -before E
                    #
            }            
        
        
        addMenu_G_after_A {
                    #
                set menu_Add  [list \
                    -label          {Add Chapter G after A} \
                    -iconImage      $iconArray(G) \
                    -command        {::updateCmd frametubes} \
                    children {
                        Add_F_01 {
                            -type       button \
                            -label      "add - Frame Details" \
                            -command    {::updateCmd frametubes_01} \
                            -tooltip    "... refine frame & frame tubes" \
                        }
                        Add_F_02 {
                            -type       button \
                            -label      "add - ChainStays Details" \
                            -command    {::updateCmd frametubes_01} \
                            -tooltip    "... refine chainstays" \
                        }
                        Add_F_03 {
                            -type       separator \
                        }
                        Add_F_04 {
                            -type       button \
                            -label      "add - check Lug Angles" \
                            -command    {::updateCmd frametubes_01} \
                            -tooltip    "... check lug-angles for building with lugs" \
                        }
                    }
                ]
                    #    
                $objAccordion_B addChapter Add_G $menu_Add    -after A
                    #
            }            
        
        custom    {
                            .f2.t insert end $id
                            set configDict [$objAccordion_B getConfigDict]
                        }
        
        default         {.f2.t insert end " ... $id ... not defined in updateCmd"}
        
    }

}
    #
    #
    # UPDATE TTK::THEMES
    # provide theme: clearlogic
    #
package require Tk                  ;# if you run this script with tclsh
update_ttkStyle                     ;# style has to be defined before accordionMenu can extend this style for its own purpose
package require accordionMenu 0.04  ;# of course to demonstrate the library
    #
package require dicttool
    #
    #
puts "    -> \$accordionMenu::packageHomeDir $accordionMenu::packageHomeDir"
    #
    #
set show_secondaryDimension 1
set show_resultDimension    0
set show_summaryDimension   0
    #
frame   .f1     -width 160  -height 600 -bg lightblue
frame   .f2     -width 640  -height 600 -bg lightgray
frame   .f3     -width 160  -height 600 -bg lightblue
frame   .f4     -width 160  -height 600 -bg lightgray
    #
pack    .f1 .f2 .f3 .f4 -side left
pack configure  .f1     -expand no  -fill y
pack configure  .f2     -expand yes -fill both
pack configure  .f3     -expand yes -fill y
pack configure  .f4     -expand yes -fill y
    #
text    .f2.t   -bd 1  -relief sunken
pack    .f2.t   -expand yes -fill both
    #       
    #       
button  .f4.b1  -bd 1  -width 25  -text "openGeometry" -command {::updateCmd openGeometry}
pack    .f4.b1  -expand yes -fill x
button  .f4.b2  -bd 1  -width 25  -text "report 01" -command {::updateCmd getConfigDict}
pack    .f4.b2  -expand yes -fill x
button  .f4.b3  -bd 1  -width 25  -text "hideEntry" -command {::updateCmd hideEntry}
pack    .f4.b3  -expand yes -fill x
button  .f4.b4  -bd 1  -width 25  -text "disableGeometry" -command {::updateCmd disableGeometry}
pack    .f4.b4  -expand yes -fill x
button  .f4.b5  -bd 1  -width 25  -text "enableGeometry" -command {::updateCmd enableGeometry}
pack    .f4.b5  -expand yes -fill x
button  .f4.b6  -bd 1  -width 25  -text "disableEntry" -command {::updateCmd disableEntry}
pack    .f4.b6  -expand yes -fill x
button  .f4.b7  -bd 1  -width 25  -text "enableEntry" -command {::updateCmd enableEntry}
pack    .f4.b7  -expand yes -fill x
button  .f4.b8  -bd 1  -width 25  -text "disableCustom" -command {::updateCmd disableCustom}
pack    .f4.b8  -expand yes -fill x
button  .f4.b9  -bd 1  -width 25  -text "enableCustom" -command {::updateCmd enableCustom}
pack    .f4.b9  -expand yes -fill x
button  .f4.b10 -bd 1  -width 25  -text "addChapter F before E" -command {::updateCmd addMenu_F_before_E}
pack    .f4.b10 -expand yes -fill x
button  .f4.b11 -bd 1  -width 25  -text "addChapter G after A" -command {::updateCmd addMenu_G_after_A}
pack    .f4.b11 -expand yes -fill x

button  .f4.b99 -bd 1  -width 25  -text "report AccordionClass" -command {::updateCmd report}
pack    .f4.b99 -expand yes -fill x
    #
    
    #
init_iconArray
    #
create_AccordionMenu_old .f1
        #
        #
    set menu_A  [list \
        A   [list \
            -label          {Project} \
            -iconImage      $iconArray(A) \
            -command        {::updateCmd project} \
            children {
                01 {
                    -type       button \
                    -label      "open Project ..." \
                    -command    {::updateCmd project_01} \
                    -tooltip    "... open Project" \
                }
                02 {
                    -type       button \
                    -label      "read Template ..." \
                    -command    {::updateCmd project_02} \
                    -tooltip    ".... read Template" \
                }
                03 {
                    -type        separator \
                }
                04 {
                    -type       button \
                    -label      "Template Road" \
                    -command    {::updateCmd project_03} \
                    -tooltip    ".... use Template Road" \
                    -status     active \
                }
                05 {
                    -type       button \
                    -label      "Template Off-Road" \
                    -command    {::updateCmd project_04} \
                    -tooltip    ".... use Template Off-Road" \
                }
            }
        ] \
    ]
    set menu_B  [list \
        B   [list \
            -label          {Bike Fitting} \
            -iconImage      $iconArray(B) \
            -command        {::updateCmd fitting} \
            children {
                B_01 {
                    -type       button \
                    -label      "enter Position ..." \
                    -command    {::updateCmd fitting_01} \
                    -tooltip    "... enter positions from bike-fitting" \
                }
            }
        ] \
    ]
    set menu_C  [list \
        C   [list \
            -label          {Geometry} \
            -iconImage      $iconArray(C) \
            -command        {::updateCmd geometry}\
            children {
                01 {
                    -type       button \
                    -label      "import Bike Fitting" \
                    -command    {::updateCmd geometry_01} \
                    -tooltip    "... import positions from bike-fitting as reference" \
                }
                02 {
                    -type       button \
                    -label      "import Frame" \
                    -command    {::updateCmd geometry_02} \
                    -tooltip    {} \
                }
                03 {
                    -type       separator \
                }
                04 {
                    -type       label \
                    -label      "Dimensions:"
                }
                05 {
                    -type       checkbutton \
                    -label      "Secondary:" \
                    -command    {::updateCmd geometry_03} \
                    -variable   {::show_secondaryDimension} \
                }
                06 {
                    -type       checkbutton \
                    -label      "Result:" \
                    -command    {::updateCmd geometry_04} \
                    -variable   {::show_resultDimension} \
                }
                07 {
                    -type       checkbutton \
                    -label      "Summary:" \
                    -command    {::updateCmd geometry_05} \
                    -variable   {::show_summaryDimension} \
                }
            }
        ] \
    ]
    set menu_D  [list \
        D [list \
            -label          {Custom 001} \
            -iconImage      $iconArray(D) \
            -command        {::updateCmd custom} \
            -status         disabled
        ] \
    ]
    set menu_E  [list \
        E [list \
            -label          {Frame & Frame Tubes} \
            -iconImage      $iconArray(E) \
            -command        {::updateCmd frametubes}\
            children {
                E_01 {
                    -type       button \
                    -label      "Frame Details" \
                    -command    {::updateCmd frametubes_01} \
                    -tooltip    "... refine frame & frame tubes" \
                }
                E_02 {
                    -type       button \
                    -label      "ChainStays Details" \
                    -command    {::updateCmd frametubes_01} \
                    -tooltip    "... refine chainstays" \
                }
                E_03 {
                    -type       separator \
                }
                E_04 {
                    -type       button \
                    -label      "check Lug Angles" \
                    -command    {::updateCmd frametubes_01} \
                    -tooltip    "... check lug-angles for building with lugs" \
                }
            }
        ] \
    ]
    #
set accordionDict "$menu_A $menu_B $menu_C $menu_D $menu_E"
    #


    # $acc add            -text "Frame & Frame Tubes"  -image $iconArray(D)   -style "IconButton"     -command [list ::updateCmd frametubes]
    #     #
    # $acc addCommand 4   -type button       -text "Frame Details"               -command [list ::updateCmd frametubes_01] -tooltip "... refine frame & frame tubes"
    # $acc addCommand 4   -type button       -text "ChainStays Details"          -command [list ::updateCmd frametubes_02] -tooltip "... refine chainstays"
    # $acc addCommand 4   -type separator    
    # $acc addCommand 4   -type button       -text "check Lug Angles"            -command [list ::updateCmd frametubes_03] -tooltip "... check lug-angles for building with lugs"
    
    
    
    
    
    #
    #
puts "--- \$accordionDict ---"
puts [dict print $accordionDict]
puts "--- \$accordionDict ---"
    #
create_AccordionMenu .f3 $accordionDict